Free Edition License
Accept? Yes


Failed to start Embedded Service ...
do /data/tmp ko 

chmod 777 /data/tmp
chmod +t /data/tmp

Sqoop2 ko co lib folder

phan cai rack chua thay noi den dat file topology.data o dau

Chú ý:
1. Thư mục chứa Parcel:
	Đổi vs phiên bản mới 4.7.1, khi tạo symbolic link để lưu Parcel sẽ gây ra lỗi
	[13/Sep/2013 14:21:48 +0000] 25814 MainThread agent        WARNING  Setting default socket timeout to 30!
	[13/Sep/2013 14:21:48 +0000] 25814 MainThread parcel       INFO     Agent creates users/groups and applies file permissions: True
	[13/Sep/2013 14:21:48 +0000] 25814 MainThread downloader   INFO     Downloader path: /opt/cloudera/parcel-cache
	[13/Sep/2013 14:21:48 +0000] 25814 MainThread parcel_cache INFO     Using /opt/cloudera/parcel-cache for parcel cache
	[13/Sep/2013 14:21:53 +0000] 25814 Monitor-HostMonitor network_interfaces INFO     NIC iface eth0 doesn't support ETHTOOL (95)
	[9/13/2013 3:26:46 PM] dinh phi:  Installation failed. Failed to receive heartbeat from agent.

	Trên cloudera manager sẽ hiển thị thông báo lỗi:
		Ensure that the host's hostname is configured properly.
		Ensure that port 7182 is accessible on the Cloudera Manager server (check firewall rules).
		Ensure that ports 9000 and 9001 are free on the host being added.
		Check agent logs in /var/log/cloudera-scm-agent/ on the host being added (some of the logs can be found in the installation details).
	
	Khắc phục: ko tạo symbolic link mà thay đổi luôn thư mục lưu trữ parcel:
	On the node side, you need to uncomment and change this line in the /etc/cloudera-scm-agent/config.ini:

		# parcel_dir=/opt/cloudera/parcels

		and then restart the agent

		service cloudera-scm-agent restart

2. File /etc/hosts phải định nghĩa theo parten sau:

	IP <FQDN> <ShortName>
	
3. 