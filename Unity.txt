

E nghĩ là nên vừa giới thiệu về Unity và giới thiệu về team Unity thì hợp lý hơn:

I- Về team:
	-	Thời gian kinh nghiệm : 1 năm
	- 	Số member ~ 10
	-	Các thể loại game đã làm: 
		2D: game card demo cho OverDriver, game bắn bóng ( giống game bắn trứn).
		3D: kiểu game Tower Defense, game Defense Island ( đã cho lên Google Play), game chiến đấu phi thuyền Galaxy Hero, game đang làm Boxing (game card)
	-	Các kỹ thuật game, plug-in đã làm:
		*	Facebook, In App Purchase trên IOS, Android (sử dụng plug in của Prime31)
		*	NGUI: Plug in hỗ trợ giao diện đồ họa 2D: thiết kế menu, tạo animation,...
		*	Tower Defense
		*	Kỹ thuật cache ảnh local, sử dụng OAuth để xác thực, ...
		* 	Kỹ thuật Unity: tương tác vật lý, tạo Shader,...
	-	

II- Vể Unity:
	-	Hỗ trợ đa môi trường: Window, Android, IOS, Mac OS, Web,...
	-	Thư viện Asset đồ sộ: của riêng Unity và của bên thứ 3 phát triển (Assets Store)
	-	Quản lý memory tốt: game chạy nuột trên các tbi di động
	-	Chủ yếu xử lý bằng kéo thả -> rất tiện
	-	Hỗ trợ 2D và 3D.
	-	Hỗ trợ tương tác vật lý 3D rất tốt.
	-	Xử lý đồ họa đẹp, mịn.
	-	Sử dụng prefab linh động.
	