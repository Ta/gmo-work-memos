Sys.setenv(JAVA_HOME="/usr/java/jdk1.6.0_31")
Sys.setenv(HADOOP_HOME="/opt/cloudera/parcels/CDH/lib/hadoop")
Sys.setenv(HADOOP_CMD="/opt/cloudera/parcels/CDH/bin/hadoop")
Sys.setenv(HADOOP_BIN="/opt/cloudera/parcels/CDH/bin")
Sys.setenv(HADOOP_STREAMING="/opt/cloudera/parcels/CDH/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.5.0.jar")   
library(rmr2)

column.names <- c("A1","A2","A3","A4","A5","A6","A7","A8","URL","A10")

bulldozer.input.format = 
        make.input.format(
                "csv",
                sep=" ",
                quote="\"",
                row.names=NULL,
                col.names=column.names,
                fill=TRUE,
                na.strings=c("NA"),
                )
				
				
map <- function(k,lines) { 
	a <- c.keyval()
	view <-"view"
	action<-"action"
	for ( url in lines$URL){
			write(url,stderr())
            params <- unlist(strsplit(url, '[?&=]'))
            viewValue <- NULL
            actionValue <- NULL
			if(length(params[2])==1){
				if(params[2]==view){
					viewValue <- params[3]
					if(length(params[4])==1){
						if(params[4]==action){
							actionValue <- params[5]
						}else{
							actionValue <- "index"
						}
					}else{
						actionValue <- "index"
					}
					a <- c.keyval(a,keyval(paste(viewValue, actionValue, sep=" "),1))
				}
			}			
	}
	a	
}


reduce <- function(word, counts) {
  keyval(word, sum(counts))
}

mapreduce(input="/tmp/log_kpi/qualia-access_log2.log", output="log_kpi_17",input.format=bulldozer.input.format, map=map, reduce=reduce,combine = T)

mapreduce(input="/tmp/longta/rf3/",
               input.format=bulldozer.input.format,
               map=poisson.subsample,
               reduce=fit.trees,
			   backend.parameters = list(hadoop  = list(D = "mapred.job.name=RandomForest",D = "mapred.reduce.tasks=3", D = "mapreduce.tasktracker.reduce.tasks.maximum=1")),
               output="/tmp/output_big005")