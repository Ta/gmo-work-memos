mapred.map.max.attempts

mapred.reduce.max.attempts

For some applications, it is undesirable to abort the job if a few tasks fail, as it may be possible to use the results of the job 
despite some failures. In this case, the maximum percentage of tasks that are allowed to fail without triggering job failure can be set for the job. 
Map tasks and reduce tasks are controlled independently, using the mapred.max.map.failures.percent and mapred.max.reduce.failures.percent properties.


mapred.max.tracker.failures