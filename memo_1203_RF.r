mahout org.apache.mahout.classifier.sgd.TrainLogistic --passes 1 --rate 1 --lambda 0.5 --input donut.csv --features 21 --output donut.model --target color --categories 2 --predictors x y xx xy yy a b c --types n n

for i in {1:10}
do
tail -10000 donut_big.csv >> donut_big.csv
done

Sys.setenv(HADOOP_HOME="/usr/bin/hadoop")
Sys.setenv(HADOOP_CMD="/usr/bin/hadoop")
Sys.setenv(HADOOP_STREAMING="/opt/cloudera/parcels/CDH-4.4.0-1.cdh4.4.0.p0.39/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.4.0.jar")
Sys.setenv(HADOOP_CONF="/home/hadoopmaster/hadoop-conf")
library(rmr2)
library(randomForest)

# PARAM
# A great advantage of RHadoop is that the R environment I'm defining here will be
# packaged and distributed to each mapper/reducer, so there is no need to mess with
# Hadoop configuration variables or distributed cache
frac.per.model <- 0.005
num.models <- 1000

# here we manually set the schema for the input data
# printed with dput(names(training.data))
column.names <- c("MachineID", "SalePrice", "ModelID.x", "datasource", "auctioneerID", 
                  "YearMade", "MachineHoursCurrentMeter", "UsageBand", "saledate", 
                  "fiModelDesc.x", "fiBaseModel.x", "fiSecondaryDesc.x", "fiModelSeries.x", 
                  "fiModelDescriptor.x", "ProductSize", "fiProductClassDesc.x", 
                  "state", "ProductGroup.x", "ProductGroupDesc.x", "Drive_System", 
                  "Enclosure", "Forks", "Pad_Type", "Ride_Control", "Stick", "Transmission", 
                  "Turbocharged", "Blade_Extension", "Blade_Width", "Enclosure_Type", 
                  "Engine_Horsepower", "Hydraulics", "Pushblock", "Ripper", "Scarifier", 
                  "Tip_Control", "Tire_Size", "Coupler", "Coupler_System", "Grouser_Tracks", 
                  "Hydraulics_Flow", "Track_Type", "Undercarriage_Pad_Width", "Stick_Length", 
                  "Thumb", "Pattern_Changer", "Grouser_Type", "Backhoe_Mounting", 
                  "Blade_Type", "Travel_Controls", "Differential_Type", "Steering_Controls", 
                  "saledatenumeric", "ageAtSale", "saleYear", "saleMonth", "saleDay", 
                  "saleWeekday", "MedianModelPrice", "ModelCount", "ModelID.y", 
                  "fiModelDesc.y", "fiBaseModel.y", "fiSecondaryDesc.y", "fiModelSeries.y", 
                  "fiModelDescriptor.y", "fiProductClassDesc.y", "ProductGroup.y", 
                  "ProductGroupDesc.y", "MfgYear", "fiManufacturerID", "fiManufacturerDesc", 
                  "PrimarySizeBasis", "PrimaryLower", "PrimaryUpper")

# here we pick the actual variables to use for building the model
# note that randomForest doesn't like missing data, so we'll just
# nix some of those variables
# TODO
model.formula <- SalePrice ~ datasource + auctioneerID + YearMade + saledatenumeric + ProductSize +
                               ProductGroupDesc.x + Enclosure + Hydraulics + ageAtSale + saleYear +
                               saleMonth + saleDay + saleWeekday + MedianModelPrice + ModelCount +
                               MfgYear
# target <- "SalePrice"
# predictors <- c("datasource", )

# here's an input format tailored for the task
bulldozer.input.format = 
        make.input.format(
                "csv",
                sep=",",
                quote="\"",
                row.names=NULL,
                col.names=column.names,
                fill=TRUE,
                na.strings=c("NA"),
                colClasses=c(MachineID="NULL",
                                                                 SalePrice="numeric",
                                                                 YearMade="numeric",
                                                                 MachineHoursCurrentMeter="numeric",
                                                                 ageAtSale="numeric",
                                                                 saleYear="numeric",
                                                                 ModelCount="numeric",
                                                                 MfgYear="numeric",
                                                                 ModelID.x="factor",
                                                                 ModelID.y="factor",
                                                                 fiManufacturerID="factor",
                                                                 datasource="factor",
                                                                 auctioneerID="factor",
                                                                 saledatenumeric="numeric",
                                                                 saleDay="factor",
                                                                 Stick_Length="numeric"))

# MAP function
poisson.subsample <- function(k, input) {
  # this function is used to generate a sample from the current block of data
  generate.sample <- function(i) {
    # generate N Poisson variables
    draws <- rpois(n=nrow(input), lambda=frac.per.model)
    # compute the index vector for the corresponding rows,
    # weighted by the number of Poisson draws
    indices <- rep((1:nrow(input)), draws)
    # emit the rows; RHadoop takes care of replicating the key appropriately
    # and rbinding the data frames from different mappers together for the
    # reducer
    keyval(i, input[indices, ])
  }
  
  c.keyval(lapply(1:num.models, generate.sample))
}

# REDUCE function
fit.trees <- function(k, v) {
  # rmr rbinds the emited values, so v is a dataframe
  # note that do.trace=T is used to produce output to stderr to keep
  # the reduce task from timing out
  #crr <- getwd()
  #write(crr,stderr())
  #install.packages(c('randomForest'), repos="http://cran.revolutionanalytics.com" )
  library('randomForest')
  rf <- randomForest(formula=model.formula, data=v, na.action=na.roughfix, ntree=10, do.trace=FALSE)
  # rf is a list so wrap it in another list to ensure that only
  # one object gets emitted. this is because keyval is vectorized
  keyval(k, list(forest=rf))
}

mapreduce(input="/tmp/longta/rf4/",
               input.format=bulldozer.input.format,
               map=poisson.subsample,
               reduce=fit.trees,
			   backend.parameters = list(hadoop  = list(D = "mapred.job.name=RandomForest",D = "mapred.reduce.tasks=20", D = "mapreduce.tasktracker.reduce.tasks.maximum=1")),
               output="/tmp/rf_big_out_05")

raw.forests <- values(from.dfs("/tmp/output"))
forest <- do.call(combine, raw.forests)
raw.forests[1,]$forest

install.packages("randomForest")


longta <- function() {
 library(randomForest)
 randomForest()
 }
