#!/bin/sh

#----------------------------------------------------------------
CURRENT_DIR=$(pwd)
LOG_DIR="${CURRENT_DIR}/logs"
QUERRY_DIR="${CURRENT_DIR}/qrs"
#----------------------------------------------------------------

if [ ! -d ${LOG_DIR} ];then
   mkdir ${LOG_DIR}
fi

if [ ! -d ${QUERRY_DIR} ];then
   mkdir ${QUERRY_DIR}
fi