







MR1: FIFO(default), Fair, Capacity
MR2: Capacity(default), FIFO

1. FIFO

2. Fair Schedule
	- Fair Schedule sẽ thiết lập cho các job resource ( trung bình). -> slot cho job
	- job được đặt trong pool, mỗi user sẽ có 1 pool. Từng pool sẽ được set số lượng resource sử dụng
	Trong mỗi pool có thể thiết lập fair sharing hoặc FIFO schedule.
	- Fair cho phép gán "minimum shares" cho pool: đảm bảo users,group hoặc app luôn nhận được đủ
	resource cần thiết (tối thiếu là bao nhiêu để thực hiện job trong pool). Nếu hệ thống không cung
	cấp đủ resource cần thiết thì Fair cũng hỗ trợ preemption ( sự mua trước) -> pool sẽ đc cho phép
	kill task của pool khác để có thể tạo room để chạy job. -> không làm job fail mà làm cho job bị kill
	task sẽ tốn nhiều time thực thi hơn.
	- Fair cũng có thể giới hạn số lượng job cho mỗi user và pool,
	- Có thể set priority cho job: cao thì sẽ được phân phối nhiều tài nguyên hơn
	- Cài đặt Fair:
		1. Copy HADOOP_HOME/build/contrib/fairscheduler to HADOOP_HOME/lib
		2. Set mapred-site.xml:
				<property>
				  <name>mapred.jobtracker.taskScheduler</name>
				  <value>org.apache.hadoop.mapred.FairScheduler</value>
				</property>
		3. Restart cluster, check on http://<jobtracker URL>/scheduler
		4. Configuration: 2 vị trí
			> mapred-site.xml ( các parameter của thuật toán).
			> HADOOP_CONF_DIR/fair-scheduler.xml: config pool,minimum shares, running 
			job limits and preemption timeouts
			
		link reference: http://hadoop.apache.org/docs/stable/fair_scheduler.html

3. Capacity Schedule
	- Chia làm nhiểu queue chạy cùng lúc.
	- Mỗi queue sẽ được phép sử dụng bao nhiêu % cluster
	- Có thể giới hạn số lượng tối đa resource cho mỗi queue
	- FIFO scheduling trong mỗi queue
	- Có hỗ trợ preemption
	- Hỗ trợ memory-intensive job (thâm canh :)) memory)
	- Hỗ trợ job priority ( disable by default)
	- Có thể hủy toàn bộ resource sử dụng bởi user trên mỗi queue nếu có cạnh tranh
	
Fair linh hoạt hơn trong config resource
Khác về nguyên lý nhưng cùng chức năng.













