Sys.setenv(HADOOP_HOME="/usr/bin/hadoop")
Sys.setenv(HADOOP_CMD="/usr/bin/hadoop")
Sys.setenv(HADOOP_STREAMING="/opt/cloudera/parcels/CDH-4.3.0-1.cdh4.3.0.p0.22/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.3.0.jar")
Sys.setenv(HADOOP_CONF="/home/hadoopmaster/hadoop-conf")
library(rmr2)
library(rhdfs)

## @knitr wordcount-signature
wordcount3 = 
  function(
    input, 
    output = NULL, 
    pattern = " "){
## @knitr wordcount-map
    wc.map = 
      function(., lines) {
	
		#write("prints to stderr AAAAAAAAAA", stderr())
		#write(ll, stderr())
        keyval(
          unlist(
            strsplit(
              x = lines,
              split = pattern)),
          1)}
## @knitr wordcount-reduce
    wc.reduce =
      function(word, counts ) {
        keyval(word, sum(counts))}
## @knitr wordcount-mapreduce
    mapreduce(
      input = input ,
      output = output,
	  backend.parameters = list(hadoop  = list(D = "mapred.job.name=longtahaha2",D = "mapred.task.timeout=6000000")),
      input.format = "text",
      map = wc.map,
      reduce = wc.reduce,
      combine = T)}
out.local = from.dfs(wordcount3("/HiBench/Wordcount/wc_in", pattern = " "))
## @knitr end
	  backend.parameters = list(hadoop  = list(D = "mapred.job.name=longtahaha2",D = "mapred.output.format.class=org.apache.hadoop.mapred.TextOutputFormat", D = "mapred.output.key.class=org.apache.hadoop.io.Text", D = "mapred.mapoutput.value.class=org.apache.hadoop.io.Text", D = "mapred.output.value.class=org.apache.hadoop.io.Text", D = "mapred.mapoutput.key.class=org.apache.hadoop.io.Text" , D = "mapred.output.compress=false")),
rmr.options(backend = "local")
file.copy("/etc/passwd", "/tmp/wordcount-test")
out.local = from.dfs(wordcount("/tmp/wordcount-test", pattern = " +"))
file.remove("/tmp/wordcount-test")
