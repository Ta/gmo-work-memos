logistic.regression = 
  function(input, iterations, dims, alpha){

## @knitr logistic.regression-map
  lr.map =          
    function(., M) {
      Y = M[,1] 
      X = M[,-1]
      keyval(
        1,
        Y * X * 
          g(-Y * as.numeric(X %*% t(plane))))}
## @knitr logistic.regression-reduce
  lr.reduce =
    function(k, Z) 
      keyval(k, Z)
## @knitr logistic.regression-main
  plane = t(rep(0, dims))
  g = function(z) 1/(1 + exp(-z))
  for (i in 1:iterations) {
    gradient = 
      values(
        from.dfs(
          mapreduce(
            testdata,
            map = lr.map,     
            reduce = lr.reduce,
            combine = T)))
	gradient
	summary(gradient)
    plane = plane + alpha * gradient }
  plane }
## @knitr end

out = list()
test.size = 10^2
for (be in c("hadoop")) {
  rmr.options(backend = be)
  ## create test set 
  set.seed(0)
## @knitr logistic.regression-data
  eps = rnorm(test.size)
  testdata = 
    to.dfs(
      as.matrix(
        data.frame(
          y = 2 * (eps > 0) - 1,
          x1 = 1:test.size, 
          x2 = 1:test.size + eps)))
## @knitr end  

  out[[be]] = 
## @knitr logistic.regression-run 
    logistic.regression(
      testdata, 1, 2, 0.05)
## @knitr end  
  ## max likelihood solution diverges for separable dataset, (-inf, inf) such as the above
}
stopifnot(
  isTRUE(all.equal(out[['local']], out[['hadoop']], tolerance = 1E-7)))