
library(randomForest)
column.names <- c("MachineID", "SalePrice", "ModelID.x", "datasource", "auctioneerID", 
                  "YearMade", "MachineHoursCurrentMeter", "UsageBand", "saledate", 
                  "fiModelDesc.x", "fiBaseModel.x", "fiSecondaryDesc.x", "fiModelSeries.x", 
                  "fiModelDescriptor.x", "ProductSize", "fiProductClassDesc.x", 
                  "state", "ProductGroup.x", "ProductGroupDesc.x", "Drive_System", 
                  "Enclosure", "Forks", "Pad_Type", "Ride_Control", "Stick", "Transmission", 
                  "Turbocharged", "Blade_Extension", "Blade_Width", "Enclosure_Type", 
                  "Engine_Horsepower", "Hydraulics", "Pushblock", "Ripper", "Scarifier", 
                  "Tip_Control", "Tire_Size", "Coupler", "Coupler_System", "Grouser_Tracks", 
                  "Hydraulics_Flow", "Track_Type", "Undercarriage_Pad_Width", "Stick_Length", 
                  "Thumb", "Pattern_Changer", "Grouser_Type", "Backhoe_Mounting", 
                  "Blade_Type", "Travel_Controls", "Differential_Type", "Steering_Controls", 
                  "saledatenumeric", "ageAtSale", "saleYear", "saleMonth", "saleDay", 
                  "saleWeekday", "MedianModelPrice", "ModelCount", "ModelID.y", 
                  "fiModelDesc.y", "fiBaseModel.y", "fiSecondaryDesc.y", "fiModelSeries.y", 
                  "fiModelDescriptor.y", "fiProductClassDesc.y", "ProductGroup.y", 
                  "ProductGroupDesc.y", "MfgYear", "fiManufacturerID", "fiManufacturerDesc", 
                  "PrimarySizeBasis", "PrimaryLower", "PrimaryUpper")
						   
						   
        test <- read.table(
                file="game.csv",
                sep=",",
                quote="\"",
                row.names=NULL,
                col.names=column.names,
                fill=TRUE,
                na.strings=c("NA"),
                colClasses=c(MachineID="NULL",
                                                                 SalePrice="numeric",
                                                                 YearMade="numeric",
                                                                 MachineHoursCurrentMeter="numeric",
                                                                 ageAtSale="numeric",
                                                                 saleYear="numeric",
                                                                 ModelCount="numeric",
                                                                 MfgYear="numeric",
                                                                 ModelID.x="factor",
                                                                 ModelID.y="factor",
                                                                 fiManufacturerID="factor",
                                                                 datasource="factor",
                                                                 auctioneerID="factor",
                                                                 saledatenumeric="numeric",
                                                                 saleDay="factor",
                                                                 Stick_Length="numeric"))

load("forest.RData")
predict(r,test2)
