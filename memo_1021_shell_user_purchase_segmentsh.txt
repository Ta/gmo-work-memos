#!/bin/sh

#--------------------------------------------------
CURRENT_DIR="/data/purchase_segment"
SETTING_FILE="${CURRENT_DIR}/SETTING.sh"
INSERT_USER_PURCHASE_SEGMENT_HIVEQL="${CURRENT_DIR}/qrs/user_purchase_segment.hiveql"
#--------------------------------------------------


if [ ! -f ${SETTING_FILE} ] ; then
   echo "Setting file not exist"
   exit 1
else
   . ${SETTING_FILE}
fi

LOG_FILE="${LOG_DIR}/user_purchase_segment_logs.log"

#if [ -f ${PURCHASE_FLAG} ] || [ -f ${ASSOCIASION_FLAG} ] ; then
#   echo "[ERROR] $(date) PAUSE JOB !!!" >> ${LOG_FILE}
#   exit 1
#fi

if [ ! -f ${USER_FLAG} ] ; then
   touch ${USER_FLAG}
fi



START_TIME=$(date +"%s")

#echo ${LOG_FILE}
cat <<_EOF_ > ${INSERT_USER_PURCHASE_SEGMENT_HIVEQL}
use private_dmp;
set mapred.job.pool.name=low_priority;
set mapred.job.name="INSERT_USER_PURCHASE_SEGMENT_HIVEQL"
drop table if exists tmp_user_purchase_segment;
create table if not exists tmp_user_purchase_segment like user_purchase_segment;
insert into table tmp_user_purchase_segment
select member_id,CASE
WHEN  birthday >= from_unixtime(unix_timestamp()-12*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-4*365*60*60*24, 'yyyyMMdd') THEN 'C'
WHEN  birthday >= from_unixtime(unix_timestamp()-19*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-12*365*60*60*24-60*60*24, 'yyyyMMdd') THEN 'T'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F3'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M3'
ELSE 'N'
END AS audience_group,category_l_id,category_m_id,category_s_id from
(select member_id,max(category_l_id) as category_l_id,max(category_m_id) as category_m_id,max(category_s_id) as category_s_id,sex,birthday from
 (select table3.member_id,category_l_id,category_m_id,category_s_id,sex,birthday from
 (select member_id,MAX(sumGroupCategory) as maxGroupCategory from
  (select member_id,category_l_id,category_m_id,category_s_id,sum(sumpurchase) as sumGroupCategory from
    (select table1.member_id,sales_price,sales_quantity,table1.category_l_id,table1.category_m_id,table1.category_s_id,sumpurchase from
     (select member_id,sales_price,sales_quantity,category_l_id,category_m_id,category_s_id, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_s_id!=0 )
         table1 LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = table1.member_id) where tmp_user_purchase_segment.member_id is null) table_sum
   group by member_id,category_l_id,category_m_id,category_s_id) table2
group by member_id) table3 join
  (select member_id,category_l_id,category_m_id,category_s_id,sex,birthday,sum(sumpurchase) as sumGroupCategory from
    (select member_master.member_id,sales_price,sales_quantity,item_master.category_l_id,item_master.category_m_id,item_master.category_s_id,sex,birthday, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_s_id!=0
       join member_master on member_master.member_id = item_purchase_history.member_id
       LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = member_master.member_id) where tmp_user_purchase_segment.member_id is null
    )
     table12
   group by member_id,category_l_id,category_m_id,category_s_id,sex,birthday) table22
on table3.maxGroupCategory = table22.sumGroupCategory and table3.member_id=table22.member_id) table4 group by member_id,sex,birthday) table5;

insert into table tmp_user_purchase_segment
select member_id,CASE
WHEN  birthday >= from_unixtime(unix_timestamp()-12*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-4*365*60*60*24, 'yyyyMMdd') THEN 'C'
WHEN  birthday >= from_unixtime(unix_timestamp()-19*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-12*365*60*60*24-60*60*24, 'yyyyMMdd') THEN 'T'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F3'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M3'
ELSE 'N'
END AS audience_group,category_l_id,category_m_id,category_s_id from
(select member_id,max(category_l_id) as category_l_id,max(category_m_id) as category_m_id,max(category_s_id) as category_s_id,sex,birthday from
 (select table3.member_id,category_l_id,category_m_id,category_s_id,sex,birthday from
 (select member_id,MAX(sumGroupCategory) as maxGroupCategory from
  (select member_id,category_l_id,category_m_id,category_s_id,sum(sumpurchase) as sumGroupCategory from
    (select table1.member_id,sales_price,sales_quantity,table1.category_l_id,table1.category_m_id,table1.category_s_id,sumpurchase from
     (select member_id,sales_price,sales_quantity,category_l_id,category_m_id,category_s_id, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_m_id!=0 and item_master.category_s_id=0 )
         table1 LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = table1.member_id) where tmp_user_purchase_segment.member_id is null) table_sum
   group by member_id,category_l_id,category_m_id,category_s_id) table2
group by member_id) table3 join
  (select member_id,category_l_id,category_m_id,category_s_id,sex,birthday,sum(sumpurchase) as sumGroupCategory from
    (select member_master.member_id,sales_price,sales_quantity,item_master.category_l_id,item_master.category_m_id,item_master.category_s_id,sex,birthday, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_m_id!=0 and item_master.category_s_id=0 
       join member_master on member_master.member_id = item_purchase_history.member_id
       LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = member_master.member_id) where tmp_user_purchase_segment.member_id is null
    )
     table12
   group by member_id,category_l_id,category_m_id,category_s_id,sex,birthday) table22
on table3.maxGroupCategory = table22.sumGroupCategory and table3.member_id=table22.member_id) table4 group by member_id,sex,birthday) table5;

insert into table tmp_user_purchase_segment
select member_id,CASE
WHEN  birthday >= from_unixtime(unix_timestamp()-12*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-4*365*60*60*24, 'yyyyMMdd') THEN 'C'
WHEN  birthday >= from_unixtime(unix_timestamp()-19*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-12*365*60*60*24-60*60*24, 'yyyyMMdd') THEN 'T'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F3'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M3'
ELSE 'N'
END AS audience_group,category_l_id,category_m_id,category_s_id from
(select member_id,max(category_l_id) as category_l_id,max(category_m_id) as category_m_id,max(category_s_id) as category_s_id,sex,birthday from
 (select table3.member_id,category_l_id,category_m_id,category_s_id,sex,birthday from
 (select member_id,MAX(sumGroupCategory) as maxGroupCategory from
  (select member_id,category_l_id,category_m_id,category_s_id,sum(sumpurchase) as sumGroupCategory from
    (select table1.member_id,sales_price,sales_quantity,table1.category_l_id,table1.category_m_id,table1.category_s_id,sumpurchase from
     (select member_id,sales_price,sales_quantity,category_l_id,category_m_id,category_s_id, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_l_id!=0 and item_master.category_m_id=0 and item_master.category_s_id=0 )
         table1 LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = table1.member_id) where tmp_user_purchase_segment.member_id is null) table_sum
   group by member_id,category_l_id,category_m_id,category_s_id) table2
group by member_id) table3 join
  (select member_id,category_l_id,category_m_id,category_s_id,sex,birthday,sum(sumpurchase) as sumGroupCategory from
    (select member_master.member_id,sales_price,sales_quantity,item_master.category_l_id,item_master.category_m_id,item_master.category_s_id,sex,birthday, (sales_quantity*sales_price) as sumpurchase
       from item_purchase_history
       join item_master on item_purchase_history.item_id = item_master.item_id
       and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id and item_master.category_l_id!=0 and  item_master.category_m_id=0 and item_master.category_s_id=0
       join member_master on member_master.member_id = item_purchase_history.member_id
       LEFT OUTER JOIN tmp_user_purchase_segment
        ON (tmp_user_purchase_segment.member_id = member_master.member_id) where tmp_user_purchase_segment.member_id is null
    )
     table12
   group by member_id,category_l_id,category_m_id,category_s_id,sex,birthday) table22
on table3.maxGroupCategory = table22.sumGroupCategory and table3.member_id=table22.member_id) table4 group by member_id,sex,birthday) table5;

insert overwrite table user_purchase_segment select * from tmp_user_purchase_segment;

_EOF_
