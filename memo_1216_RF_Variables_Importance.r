library(randomForest)
rm(list = ls())
gc()
input_data <- read.csv("access_url_count_1day_continuation_rf.csv", sep="\t")
input_data$continuation <- as.factor(input_data$continuation)
game.rf <- randomForest( continuation ~.,data=input_data, ntree=200, importance=TRUE)
imp.rf <- importance(game.rf)
imp.rf <- round(imp.rf , 3)
imp.rf.df <- data.frame(imp.rf)
imp.rf.df <- data.frame(Variable=dimnames(imp.rf.df)[[1]],MeanDecreaseGini=imp.rf.df$MeanDecreaseGini)
imp.rf.df.MeanDecreaseGini <- imp.rf.df[order(-imp.rf.df$MeanDecreaseGini), ]
rownames(imp.rf.df.MeanDecreaseGini) <- 1:nrow(imp.rf.df.MeanDecreaseGini)
write.csv(imp.rf.df.MeanDecreaseGini, file="Variables_ImportanceX1_MeanDecreaseGini.csv")

#training with X3
rm(list = ls())
gc()
input_data <- read.csv("access_url_count_3days_after_1day_continuation_rf.csv", sep="\t")
input_data$continuation <- as.factor(input_data$continuation)
game.rf <- randomForest( continuation ~.,data=input_data, ntree=200, importance=TRUE)
imp.rf <- importance(game.rf)
imp.rf <- round(imp.rf , 3)
imp.rf.df <- data.frame(imp.rf)
imp.rf.df <- data.frame(Variable=dimnames(imp.rf.df)[[1]],MeanDecreaseGini=imp.rf.df$MeanDecreaseGini)
imp.rf.df.MeanDecreaseGini <- imp.rf.df[order(-imp.rf.df$MeanDecreaseGini), ]
rownames(imp.rf.df.MeanDecreaseGini) <- 1:nrow(imp.rf.df.MeanDecreaseGini)
write.csv(imp.rf.df.MeanDecreaseGini, file="Variables_ImportanceX3_MeanDecreaseGini.csv")

#training with X7
rm(list = ls())
gc()
input_data <- read.csv("access_url_count_7days_after_3days_continuation_rf.csv", sep="\t")
input_data$continuation <- as.factor(input_data$continuation)
game.rf <- randomForest( continuation ~.,data=input_data, ntree=200, importance=TRUE)
imp.rf <- importance(game.rf)
imp.rf <- round(imp.rf , 3)
imp.rf.df <- data.frame(imp.rf)
imp.rf.df <- data.frame(Variable=dimnames(imp.rf.df)[[1]],MeanDecreaseGini=imp.rf.df$MeanDecreaseGini)
imp.rf.df.MeanDecreaseGini <- imp.rf.df[order(-imp.rf.df$MeanDecreaseGini), ]
rownames(imp.rf.df.MeanDecreaseGini) <- 1:nrow(imp.rf.df.MeanDecreaseGini)
write.csv(imp.rf.df.MeanDecreaseGini, file="Variables_ImportanceX7_MeanDecreaseGini.csv")


#training with X14
rm(list = ls())
gc()
input_data <- read.csv("access_url_count_14days_after_7days_continuation_rf.csv", sep="\t")
input_data$continuation <- as.factor(input_data$continuation)
game.rf <- randomForest( continuation ~.,data=input_data, ntree=200, importance=TRUE)
imp.rf <- importance(game.rf)
imp.rf <- round(imp.rf , 3)
imp.rf.df <- data.frame(imp.rf)
imp.rf.df <- data.frame(Variable=dimnames(imp.rf.df)[[1]],MeanDecreaseGini=imp.rf.df$MeanDecreaseGini)
imp.rf.df.MeanDecreaseGini <- imp.rf.df[order(-imp.rf.df$MeanDecreaseGini), ]
rownames(imp.rf.df.MeanDecreaseGini) <- 1:nrow(imp.rf.df.MeanDecreaseGini)
write.csv(imp.rf.df.MeanDecreaseGini, file="Variables_ImportanceX14_MeanDecreaseGini.csv")


#training with X30
rm(list = ls())
gc()
input_data <- read.csv("access_url_count_30days_after_14days_continuation_rf.csv", sep="\t")
input_data$continuation <- as.factor(input_data$continuation)
game.rf <- randomForest( continuation ~.,data=input_data, ntree=200, importance=TRUE)
imp.rf <- importance(game.rf)
imp.rf <- round(imp.rf , 3)
imp.rf.df <- data.frame(imp.rf)
imp.rf.df <- data.frame(Variable=dimnames(imp.rf.df)[[1]],MeanDecreaseGini=imp.rf.df$MeanDecreaseGini)
imp.rf.df.MeanDecreaseGini <- imp.rf.df[order(-imp.rf.df$MeanDecreaseGini), ]
rownames(imp.rf.df.MeanDecreaseGini) <- 1:nrow(imp.rf.df.MeanDecreaseGini)
write.csv(imp.rf.df.MeanDecreaseGini, file="Variables_ImportanceX30_MeanDecreaseGini.csv")