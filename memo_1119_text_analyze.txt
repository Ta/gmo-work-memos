slide massiv data share distribut memoryand concurr program bigmemori foreach
slides massive data share distributed memoryand concurrent programming foreach


for (i in 11:15) {
  writeLines(strwrap(rdmTweets[[i]]$getText(), width=73))
} 

for (i in 11:15) {
  #cat(paste("[[", i, "]] ", sep=""))
  writeLines(strwrap(myCorpus[[i]], width=73))
}

============ Original Text ==============

[[11]] Slides on massive data, shared and distributed memory,and concurrent
programming: bigmemory and foreach http://t.co/a6bQzxj5
[[12]] The R Reference Card for Data Mining is updated with functions &
packages for handling big data & parallel computing.
http://t.co/FHoVZCyk
[[13]] Post-doc on Optimizing a Cloud for Data Mining primitives, INRIA, France
http://t.co/cA28STPO
[[14]] Chief Scientist - Data Intensive Analytics, Pacific Northwest National
Laboratory (PNNL), US http://t.co/0Gdzq1Nt
[[15]] Top 10 in Data Mining http://t.co/7kAuNvuf

============== Transforming Text ==============

[[11]] slides massive data shared distributed memoryand concurrent programming
bigmemory foreach
[[12]] r reference card data mining updated functions packages handling big
data parallel computing
[[13]] postdoc optimizing cloud data mining primitives inria france
[[14]] chief scientist data intensive analytics pacific northwest national
laboratory pnnl us
[[15]] top data mining

============== Stemming Text ==============

stemCompletion()

[[11]] slide massiv data share distribut memoryand concurr program bigmemori
foreach
[[12]] r refer card data mine updat function packag handl big data parallel
comput
[[13]] postdoc optim cloud data mine primit inria franc
[[14]] chief scientist data intens analyt pacif northwest nation laboratori
pnnl us
[[15]] top data mine

---------

[[11]] slides massive data share distributed memoryand concurrent programming
foreach
[[12]] r reference card data miners updated functions package handling big data
parallel computing
[[13]] postdoctoral optimizing cloud data miners primitives inria france
[[14]] chief scientist data intensive analytics pacific northwest national pnnl
using
[[15]] top data miners

---------

[[11]] slides massive data share distributed memoryand concurrent programming
foreach
[[12]] r reference card data mining updated functions package handling big data
parallel computing
[[13]] postdoctoral optimizing cloud data mining primitives inria france
[[14]] chief scientist data intensive analytics pacific northwest national pnnl
using
[[15]] top data mining



============== Term Document ==============
TermDocumentMatrix()



============== Transforming Text ==============