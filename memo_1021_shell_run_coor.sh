#!/bin/sh
#------------------------------------------------------------------------------
JOBTRACKER="vm27801051:8021"
NAMENODE="hdfs://vm27801051:8020"
END_TIME="2013-10-30T01:00+0900"
CONCURRENCY=1
PREQUENCY_TIME=5
APP_PATH="${NAMENODE}/user/hduser/examples/apps/cron"
WORKFLOW_PATH="${NAMENODE}/user/hduser/examples/apps/shell"
JOB_CONF_PATH="/home/hduser/longta/tmp/oozie-4.0.0/examples2/apps/cron/job.properties"
#------------------------------------------------------------------------------

#Calculator start time for coordinator
MINUTE=$(date +"%M")
TIME=$PREQUENCY_TIME
MV=$(echo "${MINUTE} ${TIME}" | awk '{ print $1 % $2}')
CAL_MINUTE=$(echo "${TIME} ${MV}" | awk '{ print $1 - $2}' )
echo $CAL_MINUTE


START_TIME=$(date +"%Y-%m-%dT%H:%M" -d "-${CAL_MINUTE} minutes ago")
START_TIME="${START_TIME}+0900"

#RESULT=$(oozie job -oozie http://10.112.21.50:11000/oozie -config /home/hduser/longta/tmp/oozie-4.0.0/examples2/apps/cron/job.properties -run -Dstart="${START_TIME}" -Dend="${ENDTIME}" -Djob_status="aaaa" | grep "job:" | awk '{ print $2}' )

#Run Coordinator
RESULT=$(oozie job -oozie http://10.112.21.50:11000/oozie -config ${JOB_CONF_PATH} -Dstart="${START_TIME}" \
-DnameNode=${NAMENODE} -DjobTracker=${JOBTRACKER} \
-Doozie.coord.application.path=${APP_PATH} -Dend="${END_TIME}" \
-Dconcurrency=${CONCURRENCY} -DworkflowAppUri=${WORKFLOW_PATH} -run | grep "job:" | awk '{ print $2}' )

if [ $? -ne 0 ] ; then
        echo "ERROR:START COORDINATOR FAIL"
        exit 1;
else
        echo "Start Coordinator id=${RESULT} at $(date +"%H:%M:%S %Y%m%d")"
fi
