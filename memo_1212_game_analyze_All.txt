library(randomForest)
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX1 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X3","X7","X14","X30"))]
game.rfX1 <- randomForest( X1 ~.,data=trainingX1, ntree=50, importance=TRUE)
imp.rfX1 <- importance(game.rfX1)
imp.rfX1 <- round(imp.rfX1 , 3)
matrix.imp.rfX1 <- matrix(imp.rfX1,ncol=4)
df.imp.rfX1 <- data.frame(Variables = dimnames(imp.rfX1)[[1]] , MeanDecreaseAccuracy = matrix.imp.rfX1[,3] , MeanDecreaseGini = matrix.imp.rfX1[,4] )
df.imp.rfX1.MeanDecreaseAccuracy <- df.imp.rfX1[order(-df.imp.rfX1$MeanDecreaseAccuracy), ]
rownames(df.imp.rfX1.MeanDecreaseAccuracy) <- 1:nrow(df.imp.rfX1.MeanDecreaseAccuracy)
df.imp.rfX1.MeanDecreaseGini <- df.imp.rfX1[order(-df.imp.rfX1$MeanDecreaseGini), ]
rownames(df.imp.rfX1.MeanDecreaseGini) <- 1:nrow(df.imp.rfX1.MeanDecreaseGini)
write.csv(df.imp.rfX1.MeanDecreaseAccuracy, file="Variables_ImportanceX1_MeanDecreaseAccuracy.csv")
write.csv(df.imp.rfX1.MeanDecreaseGini, file="Variables_ImportanceX1_MeanDecreaseGini.csv")

#training with X3
rm(list = ls())
gc()
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX3 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X7","X14","X30"))]
game.rfX3 <- randomForest( X3 ~.,data=trainingX3, ntree=50, importance=TRUE)
imp.rfX3 <- importance(game.rfX3)
imp.rfX3 <- round(imp.rfX3 , 3)
matrix.imp.rfX3 <- matrix(imp.rfX3,ncol=4)
df.imp.rfX3 <- data.frame(Variables = dimnames(imp.rfX3)[[1]] , MeanDecreaseAccuracy = matrix.imp.rfX3[,3] , MeanDecreaseGini = matrix.imp.rfX3[,4] )
df.imp.rfX3.MeanDecreaseAccuracy <- df.imp.rfX3[order(-df.imp.rfX3$MeanDecreaseAccuracy), ]
rownames(df.imp.rfX3.MeanDecreaseAccuracy) <- 1:nrow(df.imp.rfX3.MeanDecreaseAccuracy)
df.imp.rfX3.MeanDecreaseGini <- df.imp.rfX3[order(-df.imp.rfX3$MeanDecreaseGini), ]
rownames(df.imp.rfX3.MeanDecreaseGini) <- 1:nrow(df.imp.rfX3.MeanDecreaseGini)
write.csv(df.imp.rfX3.MeanDecreaseAccuracy, file="Variables_ImportanceX3_MeanDecreaseAccuracy.csv")
write.csv(df.imp.rfX3.MeanDecreaseGini, file="Variables_ImportanceX3_MeanDecreaseGini.csv")

#training with X7
rm(list = ls())
gc()
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX7 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X14","X30"))]
game.rfX7 <- randomForest( X7 ~.,data=trainingX7, ntree=50, importance=TRUE)
imp.rfX7 <- importance(game.rfX7)
imp.rfX7 <- round(imp.rfX7 , 3)
matrix.imp.rfX7 <- matrix(imp.rfX7,ncol=4)
df.imp.rfX7 <- data.frame(Variables = dimnames(imp.rfX7)[[1]] , MeanDecreaseAccuracy = matrix.imp.rfX7[,3] , MeanDecreaseGini = matrix.imp.rfX7[,4] )
df.imp.rfX7.MeanDecreaseAccuracy <- df.imp.rfX7[order(-df.imp.rfX7$MeanDecreaseAccuracy), ]
rownames(df.imp.rfX7.MeanDecreaseAccuracy) <- 1:nrow(df.imp.rfX7.MeanDecreaseAccuracy)
df.imp.rfX7.MeanDecreaseGini <- df.imp.rfX7[order(-df.imp.rfX7$MeanDecreaseGini), ]
rownames(df.imp.rfX7.MeanDecreaseGini) <- 1:nrow(df.imp.rfX7.MeanDecreaseGini)
write.csv(df.imp.rfX7.MeanDecreaseAccuracy, file="Variables_ImportanceX7_MeanDecreaseAccuracy.csv")
write.csv(df.imp.rfX7.MeanDecreaseGini, file="Variables_ImportanceX7_MeanDecreaseGini.csv")


#training with X14
rm(list = ls())
gc()
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX14 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X7","X30"))]
game.rfX14 <- randomForest( X14 ~.,data=trainingX14, ntree=50, importance=TRUE)
imp.rfX14 <- importance(game.rfX14)
imp.rfX14 <- round(imp.rfX14 , 3)
matrix.imp.rfX14 <- matrix(imp.rfX14,ncol=4)
df.imp.rfX14 <- data.frame(Variables = dimnames(imp.rfX14)[[1]] , MeanDecreaseAccuracy = matrix.imp.rfX14[,3] , MeanDecreaseGini = matrix.imp.rfX14[,4] )
df.imp.rfX14.MeanDecreaseAccuracy <- df.imp.rfX14[order(-df.imp.rfX14$MeanDecreaseAccuracy), ]
rownames(df.imp.rfX14.MeanDecreaseAccuracy) <- 1:nrow(df.imp.rfX14.MeanDecreaseAccuracy)
df.imp.rfX14.MeanDecreaseGini <- df.imp.rfX14[order(-df.imp.rfX14$MeanDecreaseGini), ]
rownames(df.imp.rfX14.MeanDecreaseGini) <- 1:nrow(df.imp.rfX14.MeanDecreaseGini)
write.csv(df.imp.rfX14.MeanDecreaseAccuracy, file="Variables_ImportanceX14_MeanDecreaseAccuracy.csv")
write.csv(df.imp.rfX14.MeanDecreaseGini, file="Variables_ImportanceX14_MeanDecreaseGini.csv")


#training with X30
rm(list = ls())
gc()
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX30 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X7","X14"))]
game.rfX30 <- randomForest( X30 ~.,data=trainingX30, ntree=50, importance=TRUE)
imp.rfX30 <- importance(game.rfX30)
imp.rfX30 <- round(imp.rfX30 , 3)
matrix.imp.rfX30 <- matrix(imp.rfX30,ncol=4)
df.imp.rfX30 <- data.frame(Variables = dimnames(imp.rfX30)[[1]] , MeanDecreaseAccuracy = matrix.imp.rfX30[,3] , MeanDecreaseGini = matrix.imp.rfX30[,4] )
df.imp.rfX30.MeanDecreaseAccuracy <- df.imp.rfX30[order(-df.imp.rfX30$MeanDecreaseAccuracy), ]
rownames(df.imp.rfX30.MeanDecreaseAccuracy) <- 1:nrow(df.imp.rfX30.MeanDecreaseAccuracy)
df.imp.rfX30.MeanDecreaseGini <- df.imp.rfX30[order(-df.imp.rfX30$MeanDecreaseGini), ]
rownames(df.imp.rfX30.MeanDecreaseGini) <- 1:nrow(df.imp.rfX30.MeanDecreaseGini)
write.csv(df.imp.rfX30.MeanDecreaseAccuracy, file="Variables_ImportanceX30_MeanDecreaseAccuracy.csv")
write.csv(df.imp.rfX30.MeanDecreaseGini, file="Variables_ImportanceX30_MeanDecreaseGini.csv")