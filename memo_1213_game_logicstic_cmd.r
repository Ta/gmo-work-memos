
access_url_count_1day_continuation <- read.csv("access_url_count_1day_continuation.csv", sep="\t")
access_url_count_1day_continuation <- access_url_count_1day_continuation[ , -which(names(access_url_count_1day_continuation) %in% c("playerId"))]


ReduceNoMeaningVariable <- function(data,num){
	result <- apply(data, 2, function(r) {sum(r==0)})
	list <- names(result)
	vec <- vector()
	for ( i in list){
	   if(result[[i]]>num){
		vec <- c(vec,i)
		}
	}
	data[ , -which(names(data) %in% vec)]
}
access_data <- ReduceNoMeaningVariable(access_url_count_1day_continuation,11500)
#event_guildBattleAttackCommandの属性を使わないでロジスティック回帰実行
mylogit <- glm(continuation ~ ., data = access_data, family = binomial(link = "logit"))
summary(mylogit)

#属性の大切さに並びます（小さなければちいさいほどもっと集計的です）
model <- summary(mylogit)
result <- round(data.frame(model$coefficients),5)
result[order(result$Pr...z..), ]