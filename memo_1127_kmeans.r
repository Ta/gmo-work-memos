kmeans.mr = 
  function(
    P, 
    num.clusters, 
    num.iter, 
    combine, 
    in.memory.combine) {
## @knitr kmeans-dist.fun
    dist.fun = 
      function(C, P) {
        apply(
          C,
          1, 
          function(x) 
            colSums((t(P) - x)^2))}
## @knitr kmeans.map
    kmeans.map = 
      function(., P) {
		write("prints to stderr AAAAAAAAAA", stderr())
		write(C, stderr())
        nearest = {
          if(is.null(C)) 
            sample(
              1:num.clusters, 
              nrow(P), 
              replace = T)
          else {
            D = dist.fun(C, P)
            nearest = max.col(-D)}}
        if(!(combine || in.memory.combine))
          keyval(nearest, P) 
        else 
          keyval(nearest, cbind(1, P))}
## @knitr kmeans.reduce
    kmeans.reduce = {
      if (!(combine || in.memory.combine) ) 
        function(., P){ 
		  kmeans.reduce
          t(as.matrix(apply(P, 2, mean)))
		  }
      else 
        function(k, P) {
		  write("kmeans.reduce else", stderr())
          keyval(
            k, 
            t(as.matrix(apply(P, 2, sum))))}}
## @knitr kmeans-main-1  
    C = NULL
    for(i in 1:num.iter ) {
      C = 
        values(
          from.dfs(
            mapreduce(
              P, 
              map = kmeans.map,
              reduce = kmeans.reduce)))
      if(combine || in.memory.combine)
        C = C[, -1]/C[, 1]
## @knitr end
#      points(C, col = i + 1, pch = 19)
## @knitr kmeans-main-2
      if(nrow(C) < num.clusters) {
        C = 
          rbind(
            C,
            matrix(
              rnorm(
                (num.clusters - 
                   nrow(C)) * nrow(C)), 
              ncol = nrow(C)) %*% C) }}
        C}
		
		
		
		kmeans.mr(
      "/tmp/longta/minerva/",
      num.clusters = 3, 
      num.iter = 2,
      combine = TRUE,
      in.memory.combine = TRUE)
	  
	  
	  
	  matrix(
            rnorm(10, sd = 10), 
            ncol=2)
			kmeans.mr(
      "/tmp/longta/minerva/part-0001",
      num.clusters = 3, 
      num.iter = 5,
      combine = TRUE,
      in.memory.combine = TRUE)
	  card_num,	item_num,country,level,friend_num,card_max,attack_win,money,gacha_time
	
	kmeans.mr(
      to.dfs(P),
      num.clusters = 5, 
      num.iter = 2,
      combine = FALSE,
      in.memory.combine = FALSE)
	
	          X17        X6       X3       X9        X0      X55        X4
[1,] 37.71622 14.945946 1.783784 39.04054 2.4054054 63.37838 16.972973
[2,] 17.68455  5.960615 1.828439 13.17048 0.8407955 53.64487  5.539395
[3,] 53.82433 32.271886 1.741593 66.07313 8.6365527 71.67742 75.290815
         X38.77 X13.60921057
[1,]  648.67770     13.66557
[2,]   82.16288     10.96525
[3,] 5836.58716     13.49567



     card_num  item_num  country    level friend_num card_max attack_win  	money 		gacha_time
[1,] 53.80204 32.262512 1.741366 66.04811  8.6362461 71.67014  75.259927	5844.03344   13.49537
[2,] 17.67395  5.953651 1.828442 13.15436  0.8387046 53.63958   5.513554	81.97807  	 10.96437
[3,] 43.09836 18.278689 1.786885 46.70492  4.2459016 65.32787  32.327869	645.06377  	 13.67077
[4,] 55.48077 32.961538 1.692308 71.73077  6.8653846 73.26923  90.673077	648.85346  	 13.70370
[5,] 33.04348  8.391304 2.173913 25.65217  1.7826087 59.13043   1.652174	651.24739  	 13.63297
          
 


	==============================


logistic.regression = 
  function(input, iterations, dims, alpha){

## @knitr logistic.regression-map
  lr.map =          
    function(., M) {
      Y = M[,1] 
      X = M[,-1]
      keyval(
        1,
        Y * X * 
          g(-Y * as.numeric(X %*% t(plane))))}
## @knitr logistic.regression-reduce
  lr.reduce =
    function(k, Z) {
	  write("prints to stderr AAAAAAAAAA", stderr())
	  write(Z, stderr())
	  write("prints to stderr BBBBBBBBBB", stderr())
      keyval(k, t(as.matrix(apply(Z,2,sum))))
	  }
## @knitr logistic.regression-main
  plane = t(rep(0, dims))
  g = function(z) 1/(1 + exp(-z))
  for (i in 1:iterations) {
    gradient = 
      values(
        from.dfs(
          mapreduce(
            input,
            map = lr.map,     
            reduce = lr.reduce,
            combine = T)))
    plane = plane + alpha * gradient }
  plane }