#!/bin/sh

#--------------------------------------------------
CURRENT_DIR=$(pwd)
SETTING_FILE="${CURRENT_DIR}/SETTING.sh"
INSERT_PURCHASE_SEGMENT_MASTER_HIVEQL="${CURRENT_DIR}/qrs/insert_purchase_segment_master.hiveql"
#--------------------------------------------------

if [ ! -f ${SETTING_FILE} ] ; then
   echo "Setting file not exist"
   exit 1
else
   . ${SETTING_FILE}
fi

cat <<_EOF_ > ${INSERT_PURCHASE_SEGMENT_MASTER_HIVEQL}
set mapred.job.pool.name=low_priority;
set mapred.job.name=INSERT_PURCHASE_SEGMENT_MASTER_HIVEQL;
use private_dmp;
drop table if exists tmp_purchase_segment_master;
create table if not exists tmp_purchase_segment_master like purchase_segment_master;
insert overwrite table tmp_purchase_segment_master
select CASE
WHEN  birthday >= from_unixtime(unix_timestamp()-12*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-4*365*60*60*24, 'yyyyMMdd') THEN 'C'
WHEN  birthday >= from_unixtime(unix_timestamp()-19*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-12*365*60*60*24-60*60*24, 'yyyyMMdd') THEN 'T'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F3'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M3'
ELSE 'N'
END AS audience_group, category_l_id, category_m_id , category_s_id ,count(*) as purchase_count from item_purchase_history join member_master on member_master.member_id = item_purchase_history.member_id join item_master on item_master.item_id = item_purchase_history.item_id
and item_purchase_history.service_id = item_master.service_id and item_purchase_history.shop_id = item_master.shop_id
group by CASE
WHEN  birthday >= from_unixtime(unix_timestamp()-12*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-4*365*60*60*24, 'yyyyMMdd') THEN 'C'
WHEN  birthday >= from_unixtime(unix_timestamp()-19*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-12*365*60*60*24-60*60*24, 'yyyyMMdd') THEN 'T'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="F" THEN 'F3'
WHEN  birthday >= from_unixtime(unix_timestamp()-34*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-19*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M1'
WHEN  birthday >= from_unixtime(unix_timestamp()-49*365*60*60*24, 'yyyyMMdd') and birthday <= from_unixtime(unix_timestamp()-34*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M2'
WHEN  birthday <= from_unixtime(unix_timestamp()-49*365*60*60*24-60*60*24, 'yyyyMMdd') and sex =="M" THEN 'M3'
ELSE 'N'
END, category_l_id, category_m_id , category_s_id;

insert overwrite table purchase_segment_master select * from tmp_purchase_segment_master;
_EOF_

#cat $INSERT_PURCHASE_SEGMENT_MASTER_HIVEQL

hive -f ${INSERT_PURCHASE_SEGMENT_MASTER_HIVEQL} 
                                                                                                                                               