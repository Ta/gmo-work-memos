Sys.setenv(JAVA_HOME="/usr/java/jdk1.6.0_31")
Sys.setenv(HADOOP_HOME="/opt/cloudera/parcels/CDH/lib/hadoop")
Sys.setenv(HADOOP_CMD="/opt/cloudera/parcels/CDH/bin/hadoop")
Sys.setenv(HADOOP_BIN="/opt/cloudera/parcels/CDH/bin")
Sys.setenv(HADOOP_STREAMING="/opt/cloudera/parcels/CDH/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.5.0.jar")   
library(rmr2)
map <- function(k,lines) {        
		words.list <- strsplit(lines, ' ')
		if(length(unlist(words.list)[11])>0)
        {
            link.list <- strsplit( unlist(words.list)[11], '\\?')
            params.list <- strsplit(unlist(link.list)[2], '&')[[1]]
            viewValue <- NULL
            actionValue <- NULL
			if(grepl("view=",params.list[1])){
				viewValue <- substr(params.list[1],6,nchar(params.list[1]))
				if(grepl("action=",params.list[2])){
					actionValue <- substr(params.list[1],8,nchar(params.list[2]))
				}else{
					actionValue <- "index"
				}
				return(keyval(paste(viewValue, actionValue, sep=" "),1))
			}
			
        }
}
read.text.input.format = make.input.format(mode = 'text', format = function(con, klen) {    
    if (length(line <- readLines(con, n = 1, warn = FALSE)) > 0) { 
   line<-gsub("\"", "", line)
      return( keyval(NULL, line) )
    }
    else {
      return(NULL)
    }
} )


reduce <- function(word, counts) {
  keyval(word, sum(counts))
}




wordcount <- function (input, output=NULL) {
  mapreduce(input=input, output=output,input.format=read.text.input.format, map=map, reduce=reduce,combine = T,backend.parameters = list(hadoop  = list(D = "mapred.reduce.tasks=50")))
}

out <- wordcount("/tmp/log_kpi/qualia-access_log2.log","/tmp/streaming_05")