for i in {50..59}
do
    expect -c "
    spawn ssh root@10.112.21.$i \"ls /home/hduser;\"
    expect {
      \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\r\"
        expect \"password:\"
        send \"fdwPe9Nuk#d8v\r\"
      }
          \"root@10.112.21.$i's password:\" {
        send \"fdwPe9Nuk#d8v\r\"
      }
    }
    interact
    "

done

root@10.112.21.54's password:
新しいパスワード:
新しいパスワードを再入力してください:

for i in {54..55}
do
    expect -c "
    spawn ssh-copy-id -i ~/.ssh/id_dsa.pub longta@10.112.21.$i
    expect {
      \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\r\"
        expect \"password:\"
        send \"123456\r\"
      }
      \"longta@10.112.21.$i's password:\" {
        send \"123456\r\"
      }
    }
    interact
    "
for i in {54..55}
do
    expect -c "
    spawn ssh root@10.112.21.$i \"useradd longta; passwd longta;\"*
    expect {
      \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\r\"
        expect \"password:\"
        send \"fdwPe9Nuk#d8v\r\"
      }
          \"root@10.112.21.$i's password:\" {
        send \"fdwPe9Nuk#d8v\r\"
      }
     \"新しいパスワード:\"{
        send \"123456\r\"
        expect \"新しいパスワードを再入力してください:\r\"
        send \"123456\r\"
      }
    }
    interact
    "

done

rm: remove 通常ファイル `/tmp/sudoers'?
