
Look Alike Modeling:
Key word:
	- online adver­tis­ing com­mu­nity
	- Audi­ence­M­an­ager uses a pro­pri­etary algo­rithm called TraitWeight to dis­cover new, unique audi­ence mem­bers
	
Advan­tages of look-alike modeling
Data accu­racy: The mod­el­ing processes run at reg­u­lar inter­vals, which keeps results cur­rent and rel­e­vant. The Audi­ence­M­an­ager mod­el­ing sys­tem is always work­ing for you to find and extract new value from all your data.
Automa­tion: The model will find new users for you. As a result, you don’t have to man­age a large set of sta­tic rules for audi­ence dis­cover and creation.
Save time and reduce effort: With our mod­el­ing process, you don’t have to guess at the traits or seg­ments that may work or spend scarce resources (time, effort) on cam­paigns to dis­cover new audi­ences. The model does that for you.
Reli­a­bil­ity: Mod­el­ing works with server-side dis­cov­ery and qual­i­fi­ca­tion processes that eval­u­ate your own data and the selected third-party data that you have access to. This means you don’t have to see vis­i­tors on your site to qual­ify them for a trait.