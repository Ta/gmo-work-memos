<?xml version="1.0"?>
<allocations>
  <pool name="high_priority"> 
    <weight>3.0</weight>
    <minSharePreemptionTimeout>300</minSharePreemptionTimeout>
  </pool> 
  <pool name="medium_priority">
   <weight>2.0</weight>
  </pool>
  <pool name="low_priority"> 
    <weight>1.0</weight> 
  </pool> 
   <pool name="default"> 
    <weight>2.0</weight> 
  </pool> 
  <defaultPoolSchedulingMode>fair</defaultPoolSchedulingMode>
</allocations>