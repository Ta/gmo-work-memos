
Sys.setenv(HADOOP_HOME="/usr/bin/hadoop")
Sys.setenv(HADOOP_CMD="/usr/bin/hadoop")
Sys.setenv(HADOOP_STREAMING="/opt/cloudera/parcels/CDH-4.4.0-1.cdh4.4.0.p0.39/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.4.0.jar")
Sys.setenv(HADOOP_CONF="/home/hadoopmaster/hadoop-conf")
library(rmr2)
library(rhdfs)

column.names <- c("IP","Domain","Date","Method","ReturnCode","ID","URL","UserAgent")

        test <- read.table(
                file="web.log",
                sep=" ",
                quote="\"",
                row.names=NULL,
                col.names=column.names,
                fill=TRUE,
                na.strings=c("NA")
                )
bulldozer.input.format = 
        make.input.format(
                "csv",
                sep=" ",
                quote="\"",
                row.names=NULL,
                col.names=column.names,
                fill=TRUE,
                na.strings=c("NA"))
				
weblog.map <- function(k, input) { 
  # here is where we generate the actual sampled data
  input2 <- as.character(input$URL)
  list_len <- length(input2)
  for (i in 1:list_len){
	ac <- unlist(strsplit(input2[i],"\\?"))[2]
	key <- unlist(strsplit(ac,"\\&"))[1]
	write(key,stderr())
	#keyval(c(key,1))
  } 
  keyval(c(key,1))
}



weblog.map <- function(k, input) { 
  # here is where we generate the actual sampled data
  input2 <- as.character(input$URL)
  list_len <- length(input2)
  keyval(
          unlist(
            strsplit(
              x = input2,
              split = "\\?")),
          1)}
}

weblog.reduce <- function(k, v) { 
  keyval(k, sum(v))
}

mapreduce(input="/tmp/longta/weblog/",
               input.format=bulldozer.input.format,
               map=weblog.map,
               reduce=weblog.reduce,
			   backend.parameters = list(hadoop  = list(D = "mapred.job.name=Weblog")),
               output="/tmp/output_weblog07")
			   
			   
wordcount3 = 
  function(
    input, 
    output = NULL, 
    pattern = "\\&"){
## @knitr wordcount-map
    wc.map = 
      function(., lines) {
	
		#write("prints to stderr AAAAAAAAAA", stderr())
		#write(ll, stderr())
        keyval(
          unlist(
            strsplit(
              x = lines,
              split = pattern)),
          1)}
## @knitr wordcount-reduce
    wc.reduce =
      function(word, counts ) {
        keyval(word, sum(counts))}
## @knitr wordcount-mapreduce
    mapreduce(
      input = input ,
      output = output,
	  backend.parameters = list(hadoop  = list(D = "mapred.job.name=longtahaha2",D = "mapred.task.timeout=6000000")),
      input.format = "text",
      map = wc.map,
      reduce = wc.reduce,
      combine = T)}