

cat /proc/sys/vm/swappiness
cat /proc/sys/vm/overcommit_memory
cat /proc/sys/vm/overcommit_ratio


OS Mem 996MB(1GB)
SWAP 999MB(1GB)

1.
/proc/sys/vm/swappiness 0
/proc/sys/vm/overcommit_memory 0
cat /proc/sys/vm/overcommit_ratio 50

P1 500MB
P2 900MB
P3 600Mb
=> P2 Killed

P1 500MB
P2 600MB
P3 900Mb
=> P3 Killed