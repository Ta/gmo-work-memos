Oozie:
	-	Dựa trên Workflow Engine để thực thi workflow của job ( mapreduce và pig joc)
	- 	Là Java Web-Application tun in Java servlet-container
	-	Workflow là tập hợp các action của job, và quản lý dựa trên DAG (Direct Acyclic Graph) : action 
	thứ 2 bắt buộc phải chờ action thứ nhất kết thúc mới thực thi đc.
	-	Workflow định nghĩa bằng hPDL - XML process 
	-	Oozie có thể start job từ remote systems. Remote systems sẽ gọi callback cho Oozie khi action hoàn
	thành và Oozie sẽ thực thi action tiếp theo.
	-	Oozie bao gồm control flow nodes và action nodes.
	-	Control flow nodes: định nghĩa begin và end của workflow (start, end và fail node) và cung chấp kỹ thuật để
	điều khiển workflow execution path (decision,fork,join node).
	-	Action nodes: là<end name="[NODE-NAME]"/> kỹ thuật triggers việc thực thi, tính toán của task. Support cho: Hadoop map-reduce, 
	Hadoop file system, Pig, SSH, HTTP, eMail and Oozie sub-workflow
	
Workflow def:
1.	Control nodes:
	-	Start Control Node: <start to="[NODE-NAME]"/>
	-	End Control Node: <end name="[NODE-NAME]"/>
	-	Kill Control Node: <kill name="[NODE-NAME]">
							<message>[MESSAGE-TO-LOG]</message>
						   </kill>
	-	Decision Control Node: (lựa chọn: case,...)
							<decision name="[NODE-NAME]">
								<switch>
									<case to="[NODE_NAME]">[PREDICATE]</case>
									...
									<case to="[NODE_NAME]">[PREDICATE]</case>
									<default to="[NODE_NAME]"/>
								</switch>
							</decision>
	-	Fork and Join Control Node: dùng tương ứng theo cặp
							<workflow-app name="[WF-DEF-NAME]" xmlns="uri:oozie:workflow:0.1">
								...
								<fork name="[FORK-NODE-NAME]">
									<path start="[NODE-NAME]" />
									...
									<path start="[NODE-NAME]" />
								</fork>
								...
								<join name="[JOIN-NODE-NAME]" to="[NODE-NAME]" />
								...
							</workflow-app>
	-	

Có 3 loại job trong Oozie:
	1.	Oozie Workflow Jobs: DAGS gồm các action
	2. 	Oozie coordinator Jobs: chạy job dựa trên trigger của events (date time, data availbility)
	3. 	Oozie bundle jobs: những coordinator jobs có quan hệ đc quản lý như 1 job.
		- An Oozie bundle job can have one to many coordinator jobs
		- An Oozie coordinator job can have one to many workflow jobs
		- An Oozie workflow can have one to many actions
		- An Oozie workflow can have zero to many sub-workflows
hadoop fs -mkdir oozieProject
hadoop fs -put oozieProject/* oozieProject/