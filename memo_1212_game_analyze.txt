1=mean decrease in
accuracy, 2=mean decrease in node impurity

library(randomForest)
df.imp[order(-df.imp$good), ]
colnames(X) <- c("good", "better")
dimnames(imp2)[[1]]
round(importance(game.rf), 2)
df.imp2 <- data.frame(X =imp_matrix[,1], Y = imp_matrix[,2], Z = dimnames(imp2)[[1]])
imp_matrix <- matrix(imp2,ncol=2)
game.rf <- randomForest( X1 ~.,data=test2, ntree=100, importance=TRUE)
test <- read.csv("game3.csv", sep="\t")


"X1" "X3" "X7" "X14" "X30"
df[ , -which(names(df) %in% c("z","u"))]
Warning message:
In randomForest.default(m, y, ...) :
  The response has five or fewer unique values.  Are you sure you want to do regression?
==================

library(randomForest)
access_url_count_and_continuations <- read.csv("access_url_count_and_continuations_20131001_20131125.csv", sep="\t")
access_url_count_and_continuations$X1 <- as.factor(access_url_count_and_continuations$X1)
access_url_count_and_continuations$X3 <- as.factor(access_url_count_and_continuations$X3)
access_url_count_and_continuations$X7 <- as.factor(access_url_count_and_continuations$X7)
access_url_count_and_continuations$X14 <- as.factor(access_url_count_and_continuations$X14)
access_url_count_and_continuations$X30 <- as.factor(access_url_count_and_continuations$X30)
trainingX1 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X3","X7","X14","X30"))]
trainingX3 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X7","X14","X30"))]
trainingX7 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X14","X30"))]
trainingX14 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X7","X30"))]
trainingX30 <- access_url_count_and_continuations[ , -which(names(access_url_count_and_continuations) %in% c("player_id","X1","X3","X7","X14"))]

#training with X1
game.rfX1 <- randomForest( X1 ~.,data=trainingX1, ntree=500, importance=TRUE)
imp.rfX1 <- importance(game.rfX1)
imp.rfX1 <- round(imp.rfX1 , 3)
matrix.imp.rfX1 <- matrix(imp.rfX1,ncol=2)
df.imp.rfX1 <- data.frame(Variables = dimnames(imp.rfX1)[[1]] , IncMSE = matrix.imp.rfX1[,1] , IncNodePurity = matrix.imp.rfX1[,2] )
df.imp.rfX1.IncMSE <- df.imp.rfX1[order(-df.imp.rfX1$IncMSE), ]
rownames(df.imp.rfX1.IncMSE) <- 1:nrow(df.imp.rfX1.IncMSE)
df.imp.rfX1.IncNodePurity <- df.imp.rfX1[order(-df.imp.rfX1$IncNodePurity), ]
rownames(df.imp.rfX1.IncNodePurity) <- 1:nrow(df.imp.rfX1.IncNodePurity)
write.csv(df.imp.rfX1.IncMSE, file="df_imp_rfX1_IncMSE.csv")
write.csv(df.imp.rfX1.IncNodePurity, file="df_imp_rfX1_IncNodePurity.csv")


